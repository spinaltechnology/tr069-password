import re
import ssl
import logging
import argparse

import xml.etree.ElementTree as ET
from  xml.dom import minidom

from io import BytesIO
from functools import partial
from http.server import HTTPServer, BaseHTTPRequestHandler

def inform_response(cwmp_id):
    return """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:cwmp="urn:dslforum-org:cwmp-1-2" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap-enc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <soap:Header>
    <cwmp:ID soap:mustUnderstand="1">{cwmp_id}</cwmp:ID>
    <cwmp:HoldRequests soap:mustUnderstand="1">0</cwmp:HoldRequests>
  </soap:Header>
  <soap:Body>
    <cwmp:InformResponse>
      <MaxEnvelopes>1</MaxEnvelopes>
    </cwmp:InformResponse>
  </soap:Body>
</soap:Envelope>
""".format(cwmp_id=cwmp_id)


def empty():
    return ""


def get_parameters_values(cwmp_id, parameters):
    for name in parameters:
        parameters_to_get = "<ParameterValueStruct><Name>{name}</Name></ParameterValueStruct>".format(name=name)
    return """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:cwmp="urn:dslforum-org:cwmp-1-2" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap-enc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <soap:Header>
    <cwmp:ID soap:mustUnderstand="1">{cwmp_id}</cwmp:ID>
    <cwmp:HoldRequests soap:mustUnderstand="1">0</cwmp:HoldRequests>
  </soap:Header>
  <soap:Body>
    <cwmp:GetParameterValues>
      <ParameterNames>
        {parameters_to_get}
      </ParameterNames>
    </cwmp:GetParameterValues>
  </soap:Body>
</soap:Envelope>
""".format(cwmp_id=cwmp_id, parameters_to_get=parameters_to_get)


def set_parameters_values(cwmp_id, username, password):
    parameters = {'InternetGatewayDevice.DeviceInfo.User.1.Enable': 1,
        'InternetGatewayDevice.DeviceInfo.User.1.Username': username,
        'InternetGatewayDevice.DeviceInfo.User.1.Password': password}

    parameters_to_set = []
    for name, value in parameters.items():
        item = """<ParameterValueStruct>
    <Name>{name}</Name>
    <Value xsi:type="xsd:string">{value}</Value>
</ParameterValueStruct>""".format(name=name, value=value)
        parameters_to_set.append(item)

    return """<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap-enc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:cwmp="urn:dslforum-org:cwmp-1-0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
<soap:Header>
    <cwmp:ID soap:mustUnderstand="1">{cwmp_id}</cwmp:ID>
</soap:Header>
<soap:Body>
    <cwmp:SetParameterValues>
        <ParameterList soap-enc:arrayType="cwmp:ParameterValueStruct[{qty}]">
            {parameters_to_set}
        </ParameterList>
    <ParameterKey>n/a</ParameterKey>
    </cwmp:SetParameterValues>
</soap:Body>
</soap:Envelope>
""".format(cwmp_id=cwmp_id, parameters_to_set="\n".join(parameters_to_set), qty=len(parameters_to_set))


def pretty_xml(ugly_xml):
    if ugly_xml:
       dom = minidom.parseString(ugly_xml)
       return dom.toprettyxml(newl='', indent='  ')
    return None


class StatusMachine:
    def next(self, action, options):
        cwmp_id = options['cwmp_id']
        if action == 'Inform':
            return inform_response(cwmp_id)
        if action is None:
            parameters = ['InternetGatewayDevice.DeviceInfo.User.']
            return get_parameters_values(cwmp_id, parameters)
        if action == 'GetParameterValuesResponse':
            return set_parameters_values(cwmp_id, options['username'], options['password'])
        if action == 'SetParameterValuesResponse':
            return empty()


class Processor:
    def __init__(self, state_machine, options):
        self._state_machine = state_machine
        self._options = options
        self._options['cwmp_id'] = None

    def _update_cwmp_id(self, xml_content):
        cwmp_id = get_cwmp_id(xml_content)
        if cwmp_id:
            self._options['cwmp_id'] = cwmp_id

    def _dump_name_values(self, xml_content):
        parameter_names = extract_parameter_name_values(xml_content)
        if parameter_names:
            for k in parameter_names:
                print(k, '->', parameter_names[k])

    def process(self, body):
        action = get_cwmp_action(body)
        print_a("<<-- CPE Action: " + str(action))

        self._update_cwmp_id(body)
        self._dump_name_values(body)

        response_body = self._state_machine.next(action, self._options)
        response_action = get_cwmp_action(response_body)
        print_b("->> ACS Response Action: " + str(response_action))
        return response_body


def extract_parameter_name_values(xml_content):
    try:
        root = ET.fromstring(xml_content)
        nodes = root.findall('{http://schemas.xmlsoap.org/soap/envelope/}Body//ParameterValueStruct')
        parameters = {node[0].text:node[1].text for node in nodes}
        return parameters
    except Exception as exc:
        logging.warning("No parameter names " + str(exc))


def get_cwmp_id(xml_content):
    try:
        root = ET.fromstring(xml_content)
        body_element = root.find('{http://schemas.xmlsoap.org/soap/envelope/}Header')
        session_id = body_element[0].text
        return session_id
    except Exception as exc:
        logging.warning("No Session ID " + str(exc))


def get_cwmp_action(xml_content):
    if not xml_content or not len(xml_content):
        return None
    try:
        root = ET.fromstring(xml_content)
        body_element = root.find('{http://schemas.xmlsoap.org/soap/envelope/}Body')
        raw_action = body_element[0].tag
        if '{http://schemas.xmlsoap.org/soap/envelope/}Fault' == raw_action:
            fault_code = body_element[0][2][0][0].text
            fault_string = body_element[0][2][0][1].text
            message = "Fault code:{} string:{}".format(fault_code, fault_string)
            logging.error(message)
            return 'Fault'
        action = re.sub(r'{urn:dslforum-org:cwmp-.*}', '', raw_action)
        return action
    except Exception as exc:
        logging.warning("GET CWMP ACTION failed: " + str(exc))


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def print_a(*kwargs):
    print(bcolors.OKBLUE)
    print(*kwargs)
    print("\033[0;0m")

def print_b(*kwargs):
    print(bcolors.OKGREEN)
    print(*kwargs)
    print("\033[0;0m")


class ACSHTTPRequestHandler(BaseHTTPRequestHandler):

    def __init__(self, processor, *args, **kwargs):
        # functools partial magic
        self._processor = processor
        super().__init__(*args, **kwargs)

    def _body(self):
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)
        return body

    def _response(self, content):
        response = BytesIO()
        response.write(content.encode('utf8'))
        self._set_response(response.getvalue())

    def _set_response(self, response):
        self.send_response(200)
        self.send_header('Content-length', len(response))
        self.send_header('Content-type', 'text/xml; charset="UTF-8"')
        self.send_header('Server', 'Jetty(9.3.23.v20180228)')
        if self.headers.get('Connection'):
            if 'Keep-Alive' in self.headers['Connection']:
                self.send_header("Connection", "Keep-Alive")
        self.end_headers()
        self.wfile.write(response)

    def do_POST(self):
        body = self._body()

        print("POST")
        #print("A POST", self.headers.__dict__, body )
        response = self._processor.process(body)
        print("Response", pretty_xml(response))

        self._response(response)
        return


def start_simplehttp(enable_ssl, port, username, password):
    status_machine = StatusMachine()
    options={'username': username, 'password': password}
    processor = Processor(status_machine, options)
    http_handler = partial(ACSHTTPRequestHandler, processor)

    httpd = HTTPServer(('0.0.0.0', port), http_handler)

    print ("Starting ACS server at", port)
    if enable_ssl:
        print("Enabled SSL")
        print("Generate cert files with")
        print("# openssl req -x509 -nodes -newkey rsa:2048 -keyout key.pem -out cert.pem -days 365")
        httpd.socket = ssl.wrap_socket(httpd.socket,
                                       keyfile="key.pem",
                                       certfile='cert.pem',
                                       server_side=True)

    httpd.serve_forever()



DEFAULT_USER='admin'
DEFAULT_PASSWORD='password'
DEFAULT_PORT=10302

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--username',  action='store', default=DEFAULT_USER, help='username to set')
parser.add_argument('--password',  action='store', default=DEFAULT_PASSWORD, help='password to set')
parser.add_argument('--port',  action='store', default=DEFAULT_PORT, help='server port')
parser.add_argument('--ssl', action='store_true')
args = parser.parse_args()

start_simplehttp(enable_ssl=args.ssl, port=args.port, username=args.username, password=args.password)
